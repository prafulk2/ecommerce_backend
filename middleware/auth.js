const jwt = require("jsonwebtoken");
const User = require("../models/User");

// const authenticate = async (req, res, next) => {
//   const token = req.headers.authorization?.split(" ")[1] || ""

//   try {
//     const verified = jwt.verify(token, process.env.JWT_SECRET)
//     req.verifiedUser = verified.user
//     console.log("Verification success!")

//     next()      
//   } catch (err) {
//     next()  
//   }
// }


// const verifyToken = (req,res,next)=>{
//     const authHeader = req.headers.token;
//     if(authHeader){
//         const token = req.headers.authorization?.split(" ")[1] 
//         jwt.verify(token, process.env.JWT_SECRET,(err,user)=>{
//             console.log("error",err);
//             if (err) {
//                 // res.status(403).json("token is not valid");
//             }
//             req.user=user
//             next();
//         });
//         // req.user=verified.user
//     }
//     // else{
//     //     return res.status(401).json("you are not authenticated")
//     // }
// }

const Authenticate = (req, res, next) => {
  const token = req.headers.authorization?.split(" ")[1] || ""
console.log("teteegy");
  try {
    const verified = jwt.verify(token, process.env.JWT_SECRET)
    console.log("verified");
    req.user = verified.user
    console.log("Verification success!")

    next()      
  } catch (err) {
    console.log("err",err);
    // res.status(403).json(err);
    next(err)  
  }
}

const varifyTokenAndAuthorization=(req,res,next)=>{
    Authenticate(req, res, async () => {
        console.log("_id",req.user._id);
        console.log("id",req.params.id);

        try{
          if (req.user._id === req.params.id || req.user.isAdmin) {
            next();
          }
          else{
            // res.status(403).json("You are not alowed to do that!");
            next(new Error("You are not alowed to do that!"))
          }
        }
        catch(err){
          next(err)
        }
      });
}

const verifyTokenAndAdmin = (req, res, next) => {
    Authenticate(req, res, () => {
      if (req.user.isAdmin) {
        next();
      } else {
        res.status(403).json("You are not alowed to do that!");
        // next(err)
      }
    });
  };

module.exports = { Authenticate ,varifyTokenAndAuthorization,verifyTokenAndAdmin}