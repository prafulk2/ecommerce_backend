const dotenv = require("dotenv")
const express = require("express")
const app = express();
const { connectDB } = require("./db")
const auth = require("./routes/auth")
const users=require('./routes/user')
const products=require('./routes/products')
const order=require('./routes/order')
const stripeRoute = require("./routes/stripe");
const cors = require('cors')

dotenv.config()
connectDB()
 app.use(cors())
app.use(express.json())
app.use('/auth',auth)
app.use('/users',users)
app.use('/products',products)
app.use('/orders',order)
app.use('/orders',order)
app.use("/checkout", stripeRoute);


app.listen(process.env.PORT || 4000, () => {
    console.log(`App running on PORT ${process.env.PORT}`)
}) 