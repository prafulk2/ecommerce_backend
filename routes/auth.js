const router = require('express').Router()
const User=require("../models/User")
const CryptoJs=require("crypto-js")
const jwt = require("jsonwebtoken")
const { createJwtToken } = require('../util/CreateJWT')
router.post("/register",async (req,res)=>{
    const newUser= new User({
        username:req.body.username,
        email:req.body.email,
        password:CryptoJs.AES.encrypt(req.body.password,process.env.PASS_SEC),
    })
    try{
        const saveuser= await newUser.save()
        res.status(200).json(saveuser)
        console.log(saveuser);
    }catch(err){
        res.status(500).json(err)
        console.log(err);
    }
})

router.post("/login",async(req,res,next)=>{
    try{
        const user = await User.findOne({username:req.body.username});
        !user && res.status(401).json("Wrong credentials")
        const hashPassword=CryptoJs.AES.decrypt(user.password,process.env.PASS_SEC)
        const passwords=hashPassword.toString(CryptoJs.enc.Utf8);

        passwords!==req.body.password && 
            res.status(401).json("wrong credentilas!")
            const accessToken=createJwtToken(user)
            const {password , ...text} = user._doc
        res.status(200).json({...text,accessToken});
    }
    catch(err){
        // res.status(401).json("wrong credentilas!")
        // res.status(500).json(err)
        next(err)
    }
})
module.exports = router   